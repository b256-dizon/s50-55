import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';
import {Navigate} from 'react-router-dom';

export default function Register(){

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [Password1, setPassword1] = useState("");
	const [Password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	console.log(email);

	useEffect (() => {
		if ((email !== '' && Password1 !== '' && Password2 !== '') && (Password1 === Password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	})

	function registerUser(e) {
		e.preventDefault();

		localStorage.setItem("email", email)

		setUser({
			email: localStorage.getItem('email')
		})

		setEmail('');
		setPassword1('');
		setPassword2('');

		alert('Thank you for registering!');
	}

	return (
		(user.email !== null) ?
			<Navigate to="/Courses" />
		:
		
		<Form onSubmit={e => registerUser(e)}>
	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={Password1} onChange={e => setPassword1(e.target.value)}/>
	      </Form.Group>
	      <Form.Group className="mb-3" controlId="formBasicPassword2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control type="password" placeholder="Verify Password" value={Password2} onChange={e => setPassword2(e.target.value)}/>
	      </Form.Group>
	      {
	      	isActive ?
	      	<Button variant="primary" type="submit" id="submitBtn">
	        Submit
	      	</Button>
	      	:
	      	<Button variant="danger" type="submit" id="submitBtn" disabled>
	        Submit
	      	</Button>
	      }
	    </Form>
	)
}