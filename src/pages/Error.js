// Activity s53
import Banner from '../components/Banner';

export default function Error() {
	const data = {
		title: "404 - Not Found",
		content: "The page you are looking for is not available.",
		destination: "/",
		label: "Homepage"
	}
	return (
		<>
			<Banner banner={data} />
		</>
	)
}