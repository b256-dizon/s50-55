import CourseData from '../data/CourseData';
import CourseCard from '../components/CourseCard';

export default function Courses() {

	console.log(CourseData);

	const courses = CourseData.map(course => {
		return(
			<CourseCard key={course.id} courseProp={course} />
		)
	})

	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}