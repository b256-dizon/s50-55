import './App.css';
import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import { UserProvider } from './userContext';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Logout from './pages/Logout';
// Activity s53
import Error from './pages/Error';
import {useState, useEffect} from 'react';

function App() {

  const [user, setUser] = useState({email: localStorage.getItem('email')});

  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      {/* Self Closing Tags */}
      <AppNavBar /> 
      <Container>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/Courses" element={<Courses />} />
        <Route path="/Register" element={<Register />} />
        <Route path="/Login" element={<Login />} />
        <Route path="/Logout" element={<Logout />} />
        {/*  Activity s53 ----- */}
        <Route path="*" element={<Error />} />
      </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
