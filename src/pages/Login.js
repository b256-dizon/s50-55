import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';
import {Navigate} from 'react-router-dom';

export default function Login() {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [Password, setPassword] = useState("");
	const [button, setButton] = useState(false);

	useEffect(() => {
		if (email !== '' && Password !== '') {
			setButton(true);
		}
	})

	function login(e) {
		e.preventDefault();

		localStorage.setItem("email", email)

		setUser({
			email: localStorage.getItem('email')
		})

		setEmail('');
		setPassword('');
		alert("Successfully Log-in!");
	}

	return (
		(user.email !== null) ?
			<Navigate to="/Courses" />
		:

		<Form onSubmit={e => login(e)}>
	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email:</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password:</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={Password} onChange={e => setPassword(e.target.value)}/>
	      </Form.Group>
	      {
	      	button ?
	      	<Button variant="primary" type="submit">
	        Submit
	      	</Button>
	      	:
	      	<Button variant="warning" type="submit" disabled>
	        Submit
	      	</Button>
	      }
	    </Form>
	)
}