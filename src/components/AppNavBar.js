// React Bootstrap Components
import {Container, Nav, Navbar} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom'; 
import { useContext } from 'react';
import UserContext from '../userContext';


export default function AppNavBar() {

	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
	      <Container>
	        <Navbar.Brand as={Link} to="/">Zuitt Bootcamp</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="me-auto">
	            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
	            <Nav.Link as={NavLink} to="/Courses">Courses</Nav.Link>
	            {
	            	(user.email !== null) ?
	            		<Nav.Link as={NavLink} to="/Logout">Logout</Nav.Link>
	            		
	            	:
	            	<>
	            		<Nav.Link as={NavLink} to="/Register">Register</Nav.Link>
	            		<Nav.Link as={NavLink} to="/Login">Login</Nav.Link>
	            	</>
	            }
	            
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}